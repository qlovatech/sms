package sms

import "io"

//Error type for the package.
type Error string

func (err Error) Error() string { return string(err) }

//Errors defined by the package.
const (
	FormatNotSupported Error = "format not supported"
	InvalidDestination Error = "invalid destination"
)

//Message abstraction.
type Message struct {
	To, From Destination

	//Format of the Body.
	Format Format

	Header, Body string

	Attachments []Attachment
}

//Attachment abstraction
type Attachment struct {
	Name     string
	Format   string
	ReadFrom io.Reader
}

//Sender can send Messages.
type Sender interface {
	Send(Message) error
}

//Transport is the name of the technology behind the message being sent.
type Transport string

//Known transports.
const (
	Email Transport = "email"
	SMS   Transport = "sms"
)

//Format is the name of the format of the encoded body of the message. Generally a mimetype.
type Format string

//Known formats.
const (
	Text Format = "text/plain"
	HTML Format = "text/html"
)
