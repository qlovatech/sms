# SMS

Qlovastore is a messaging abstraction module for Go. Dealing both with sms-messaging and email.

**Email Drivers:**  

* Sendgrid (sendgrid)

**File-system Example:**  

```Go
package main

import (
	"fmt"

	"qlova.tech/sms"
	"qlova.tech/sms/driver/sendgrid"
)

func main() {
	sendgrid.Register(`SENDGRID_KEY`)

	var message = sms.Message{
		From: "from@example.com",

		Header: "Test Message",
		Body:   "Hello World",
	}

	if err := message.SendTo("to@example.com"); err != nil {
		fmt.Println(err)
	}
}

```

**License**  
This work is subject to the terms of the Qlova Public
License, Version 2.0. If a copy of the QPL was not distributed with this
work, You can obtain one at https://license.qlova.org/v2

The QPL is compatible with the AGPL which is why both licenses are provided in this repository.