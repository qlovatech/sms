package sms

import "strings"

var senders = make(map[Transport][]Sender)

//RegisterSenderFor registers a sender to handle the sending of messages on a given transport.
func RegisterSenderFor(t Transport, s Sender) {
	senders[t] = append(senders[t], s)
}

//Destination is a reference on how to send the message.
type Destination string

//Transport returns a transport for this destination if known.
func (d Destination) Transport() Transport {
	if strings.Contains(string(d), "@") {
		return Email
	}

	return ""
}

//SendTo sends the message to the given destination.
func (msg Message) SendTo(to Destination) error {
	msg.To = to
	return msg.Send()
}

//Send sends the message.
func (msg Message) Send() error {
	for _, sender := range senders[msg.To.Transport()] {
		return sender.Send(msg)
	}
	return Error("no sender registered for " + msg.To.Transport() + " transport")
}
