package sendgrid

import (
	"encoding/json"
	"errors"
	"strings"

	"qlova.tech/sms"

	"github.com/sendgrid/sendgrid-go"

	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

//Sender sends messages with the Sendgrid service.
type Sender struct {
	*sendgrid.Client
}

//NewSender returns a sms compatible Sender.
func NewSender(key string) Sender {
	return Sender{sendgrid.NewSendClient(key)}
}

//Register registers an email sender using the given key.
func Register(key string) {
	sms.RegisterSenderFor(sms.Email, Sender{sendgrid.NewSendClient(key)})
}

//Send attempts to send the given sms.Message
func (s Sender) Send(msg sms.Message) error {
	if !strings.Contains(string(msg.To), "@") {
		return sms.InvalidDestination
	}
	if msg.Format == "" {
		msg.Format = sms.Text
	}

	email := mail.NewV3Mail()

	p := mail.NewPersonalization()
	p.AddTos(mail.NewEmail("", string(msg.To)))

	email.AddPersonalizations(p)
	email.SetFrom(mail.NewEmail("", string(msg.From)))

	email.Subject = msg.Header
	email.AddContent(&mail.Content{
		Type:  string(msg.Format),
		Value: msg.Body,
	})

	response, err := s.Client.Send(email)
	if err != nil {
		return err
	}

	//Error is an internal struct for detecting errors.
	type Error struct {
		Message string `json:"message"`
	}

	//Response is an internal struct for detecting errors.
	type Response struct {
		Errors []Error `json:"errors"`
	}

	var r Response
	json.NewDecoder(strings.NewReader(response.Body)).Decode(&r)

	if len(r.Errors) > 0 {
		return errors.New(r.Errors[0].Message)
	}

	return err
}
