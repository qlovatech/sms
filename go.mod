module qlova.tech/sms

go 1.13

require (
	github.com/sendgrid/rest v2.6.0+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.6.1+incompatible
)
